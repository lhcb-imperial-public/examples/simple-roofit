# Simple RooFit

This is a tutorial for how to run RooFit. It fits a gaussian signal and exponential background to some test data, then plots the fit and pull histogram. Read the code in python/runme.py

## Running
  * source setup.sh # This is if you are running on the imperial machines
  * python python/runme.py
